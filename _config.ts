import lume from "lume/mod.ts";
import jsx from "lume/plugins/jsx.ts";
import attributes from "lume/plugins/attributes.ts";
import code_highlight from "lume/plugins/code_highlight.ts";
import feed from "lume/plugins/feed.ts";
import mdx from "lume/plugins/mdx.ts";
import pagefind from "lume/plugins/pagefind.ts";
import tailwindcss from "lume/plugins/tailwindcss.ts";
import postcss from "lume/plugins/postcss.ts";
import typography from "npm:@tailwindcss/typography";

const site = lume();

site.use(jsx());
site.use(attributes());
site.use(code_highlight());
site.use(feed({
  output: ["/feed.json", "/feed.xml"],
  query: "type=posts",
  info: {
    title: "=site.title",
    description: "=site.description",
  },
  items: {
    title: "=title",
    content: "$.post-body",
  },
}));
site.use(mdx());
site.use(pagefind());
site.use(tailwindcss({
  options: {
    plugins: [typography],
  },
}));
site.use(postcss());

export default site;
