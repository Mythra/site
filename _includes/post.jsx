export default ({ title, date, children }) => (
  <html>
    <head>
      <title>{title}</title>
      <link rel="stylesheet" href="/styles.css" />
    </head>
    <body>
      <div className="p-4 max-w-xl">
        <div className="p-4 mb-4 bg-gray-100">
          <nav className="flex flex-row items-center">
            <a
              className="text-lg text-blue-700 hover:text-blue-500 px-4"
              href="/"
            >
              Hikari
            </a>
            <span className="mx-auto"></span>
            <a
              className="px-4 text-blue-700 hover:text-blue-500"
              href="/search/"
            >
              Search
            </a>
          </nav>
        </div>
        <article className="prose" data-pagefind-body>
          <h1>{title}</h1>
          <small>Posted on {date.toDateString()}</small>
          {children}
        </article>
      </div>
    </body>
  </html>
);
